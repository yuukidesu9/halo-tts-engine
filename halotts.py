import numpy as np
import soundfile as sf
import yaml
# Import TensorFlow dependencies
import tensorflow as tf
from tensorflow_tts.inference import TFAutoModel
from tensorflow_tts.inference import AutoProcessor

# Initialize models and inference
fastspeech2 = TFAutoModel.from_pretrained("tensorspeech/tts-fastspeech2-ljspeech-en")
mb_melgan = TFAutoModel.from_pretrained("tensorspeech/tts-mb_melgan-ljspeech-en")
processor = AutoProcessor.from_pretrained("tensorspeech/tts-fastspeech2-ljspeech-en")

def inference(text):
      # Convert text into sequence of words.
      input_ids = processor.text_to_sequence(text)

      # FastSpeech2 inference.

      mel_before, mel_after, duration_outputs, _, _ = fastspeech2.inference(
          input_ids=tf.expand_dims(tf.convert_to_tensor(input_ids, dtype=tf.int32), 0),
          speaker_ids=tf.convert_to_tensor([0], dtype=tf.int32),
          speed_ratios=tf.convert_to_tensor([1.0], dtype=tf.float32),
          f0_ratios =tf.convert_to_tensor([1.0], dtype=tf.float32),
          energy_ratios =tf.convert_to_tensor([1.0], dtype=tf.float32),
      )

      # MelGAN inference
      audio_before = mb_melgan.inference(mel_before)[0, :, 0]
      audio_after = mb_melgan.inference(mel_after)[0, :, 0]

      # Save sound files
      sf.write('./audio_before.wav', audio_before, 22050, "PCM_16")
      sf.write('./audio_after.wav', audio_after, 22050, "PCM_16")
      return

text_in = input("Enter the text to be generated: \n")
inference(text_in)
